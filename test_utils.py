import os
import time
import datetime
import re
import json
import contextlib

# Global general variables
TIME_FORMAT = "[%H:%M:%S]"
DATE_FORMAT = "%y_%m_%d"
DEFAULT_OUTPUT_TAG = "default"
DEFAULT_TEST_SETTINGS = "default_settings.json"
PROXY_BASE_DIR = "PROXY_BASE_DIR"
TEXTURES_DIRECTORY = "media/textures"
RENDER_SETTINGS = {
    "samples": "-s",
    "bounces": "-b",
    "camera": "-c",
    "camera_type": "--camera-type",
    "resolution": "-r",
    "filter": "-f",
    "layer": "-l",
    "pass": "-p",
    "pixel_sample_clamp": "--pixel-sample-clamp",
    "ignore_transparency_bounces": "--ignore-straight-bounces"
}

# Print functions


def print_error(msg):
    time_str = time.strftime(TIME_FORMAT, time.localtime(time.time()))
    print time_str + " ERROR > " + msg


def print_warning(msg):
    time_str = time.strftime(TIME_FORMAT, time.localtime(time.time()))
    print time_str + " WARNING > " + msg


def print_info(msg):
    time_str = time.strftime(TIME_FORMAT, time.localtime(time.time()))
    print time_str + " INFO > " + msg


def inline_print_info(msg):
    time_str = time.strftime(TIME_FORMAT, time.localtime(time.time()))
    print time_str + " INFO > " + msg + "                                                            \r",


def get_current_day(date_format):
    return time.strftime(date_format, time.localtime(time.time()))


def get_current_time():
    return time.time()


def duration_from(given_time):
    return get_current_time() - given_time


def pretty_str_time(input_time):
    return str(datetime.timedelta(seconds=input_time)).split(".")[0]

# Files and directories management functions


def safe_mkdirs(dir_path):
    if dir_path == '':
        return
    if not os.path.isdir(dir_path):
        os.makedirs(dir_path)
        print_info("< Created directory \"" + dir_path + "\" >")


def file_exists_or_quit(file_path):
    if not os.path.isfile(file_path):
        print_error("File doesn't exist: " + file_path)
        exit(-1)
    else:
        print_info("< Required file found: " + file_path + " >")


def dir_exists_or_quit(dir_path):
    if not os.path.exists(dir_path):
        print_error("Directory doesn't exist: " + dir_path)
        exit(-1)
    else:
        print_info("< Required directory found: " + dir_path + " >")


def load_json_file(file, exception_behavior="do_nothing"):
    try:
        fd = open(file)
        j = json.load(fd)
        fd.close()
        return j
    except IOError as e:
        print_error("Unable to find file: " + file +
                    " (Error Message : " + str(e) + ")")
        if exception_behavior is "quit":
            exit(-1)
    except ValueError as e:
        print_error("Unable to load file as json: " + file +
                    " (Error Message : " + str(e) + ")")
        if exception_behavior is "quit":
            exit(-1)

    return None


def get_or_create_from_key(my_dict, key):
    if my_dict.get(key) is None:
        my_dict[key] = {}
    return my_dict[key]


def merge_keys(key, subkey):
    return key + ":" + subkey


def test_key(cat, group, test):
    return merge_keys(merge_keys(cat, group), test)


@contextlib.contextmanager
def pushd(new_dir):
    previous_dir = os.getcwd()
    os.chdir(new_dir)
    yield
    os.chdir(previous_dir)

# Utility Classes


class FrameRange:
    def __init__(self, settings):
        frame_range = settings["frame_range"]
        self.start = float(frame_range["start"])
        self.istart = int(self.start)
        self.end = float(frame_range["end"]) + 1.0
        self.iend = int(self.end)
        self.step = int(frame_range["step"])
        self.shutter = float(frame_range["shutter"])
        self.shutter_offset = float(frame_range["shutter_offset"])

    def print_lite(self):
        return "Frame Range: {} -> {} (shutter: {})".format(self.start, self.end, self.shutter)


class TestFilter:
    def __init__(self, user_filter):
        if user_filter is None:
            user_filter = ""

        self.regexp = "^"
        splitted = user_filter.split(":")
        forbidden_char = ["", "*", " "]
        for i in range(0, 3):
            if len(splitted) > i and splitted[i] not in forbidden_char:
                if splitted[i].startswith("~"):
                    self.regexp += "(?!" + splitted[i][1:] + ")"
                else:
                    self.regexp += "(?=" + splitted[i] + ")"

            self.regexp += "[\\w]+"

            if i < 2:
                self.regexp += ":"
        self.regexp += "$"

    def match(self, category, group, test):
        current_test = category + ":" + group + ":" + test
        return re.match(self.regexp, current_test) is not None

# Test reading utilities


def scanTestDescriptionsFromJsonFiles(test_dir, default_settings, filter_str):
    test_filter = TestFilter(filter_str)

    test_descriptions = []
    for path, dirs, files in os.walk(test_dir):
        for f in files:
            if f.endswith(".json") and f != DEFAULT_TEST_SETTINGS:
                json_file = os.path.join(path, f)
                category, group = parse_from_json_path(json_file)
                group_json = load_json_file(json_file)
                if group_json is None:
                    continue

                try:
                    common_settings = merge_settings(
                        default_settings, group_json["common"])
                    test_list = group_json["tests"]
                    for test_name, test_settings in group_json["tests"].items():
                        if test_filter.match(category, group, test_name):
                            final_settings = merge_settings(
                                common_settings, test_settings)
                            if final_settings["active"]:
                                desc = {}
                                desc["test"] = test_name
                                desc["group"] = group
                                desc["category"] = category
                                desc["settings"] = final_settings
                                test_descriptions.append(desc)

                except KeyError as e:
                    print_error("Wrong format for \"" + json_file +
                                "\" (Missing key: " + str(e) + ")")

    return test_descriptions


def parse_from_json_path(file_path):
    splitted_path = os.path.abspath(file_path).split(os.sep)
    category = splitted_path[-2]
    group = os.path.splitext(splitted_path[-1])[0]
    return category, group


def merge_settings(base, override):
    merged = base.copy()

    # add key,value from override that are not in the base
    for key in (key for key in override if not (key in base)):
        merged[key] = override[key]

    for key in (keys for keys in base if keys in override):
        if not isinstance(base[key], dict):
            merged[key] = override[key]
        else:
            merged[key] = base[key].copy()
            # add key,value from override[key] that are not in base[key]
            for subKey in (subkeys for subkeys in override[key] if not (subkeys in base[key])):
                merged[key][subKey] = override[key][subKey]

            for subKey in (subkeys for subkeys in base[key] if subkeys in override[key]):
                merged[key][subKey] = override[key][subKey]

    return merged
