window.onload = function()
{
    var checkbox = document.getElementById("display_error_checkbox");
    checkbox.onchange = function() {
        var elements = document.querySelectorAll(
                ".tests_test a:not(.in_error), .test_tr:not(.in_error), .pass_tr:not(.in_error), .frame_tr:not(.in_error)");
        if(this.checked)
        {
            for (var i = 0; i < elements.length; i++) {
                elements[i].style.display = "none";
            }
        }
        else
        {
            for (var i = 0; i < elements.length; i++) {
                elements[i].style.display = null;
            }
        }
    };
};